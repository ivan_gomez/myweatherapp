/*
 * Copyright (C) 2016 Ivan Gomez Castellanos <ivangomezc@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.example.ivan.myweatherapp;

import java.util.Calendar;

public class WeatherNoSingleton implements WeatherInterface {
    private float temp;			// Temperature (°C)
    private float windSpeed;	// Wind speed (km/h)
    private float realFeel;		// Real Feel (°C)
    private float humidity;		// Humidity (%)

    public WeatherNoSingleton() {
        temp = 0;
        windSpeed = 0;
        realFeel = 0;
        humidity = 0;
    }

    // Implement WeatherInterface methods.

    // Connect to the server to download weather info based on location and time.
    // Return true if data was downloaded successfully, or false otherwise.
    public boolean getWeather(String location, Calendar date) {
        // Get weather from server...
        // This function should block until data is received or and error happen.
        temp = 15;
        realFeel = 20;
        windSpeed = 20;
        humidity = 50;

        return true;
    }

    // Get Temperature
    public float getTemp() {
        return temp;
    }

    // Get real feel
    public float getRealFeel() {
        return realFeel;
    }

    // Get Wind speed
    public float getWindSpeed() {
        return windSpeed;
    }

    // Get Humidity
    public float getHumidity() {
        return humidity;
    }


    // other useful methods here
    public String getDescription() {
        return "Weather No Singleton!";
    }
}