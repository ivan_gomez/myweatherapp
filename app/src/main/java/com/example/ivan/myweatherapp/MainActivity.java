/*
 * Copyright (C) 2016 Ivan Gomez Castellanos <ivangomezc@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.example.ivan.myweatherapp;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private String tag = "MyWeatherApp";
    private String location;
    private WeatherInterface weather;
    private Calendar date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView weatherTextView = (TextView) findViewById(R.id.weatherInfo);

        final ToggleButton singletonToggle = (ToggleButton) findViewById(R.id.singletonSwitch);
        singletonToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled: Use Singleton
                    Log.d(tag, "Singleton Enabled");
                } else {
                    // The toggle is disabled: Do not use singleton
                    Log.d(tag, "Singleton Disabled");
                }
            }
        });

        final Button weatherButton = (Button) findViewById(R.id.getWeatherButton);
        weatherButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (singletonToggle.isChecked()) {
                    Log.d(tag, "Button pressed: Singleton Enabled");
                    weather = WeatherSingleton.getInstance();
                } else {
                    Log.d(tag, "Button pressed: Singleton Disabled");
                    weather = new WeatherNoSingleton();
                }

                date = getDate();
                location = getLocation();
                Log.d(tag, "Date: " + date.toString());
                Log.d(tag, "Location : " + location);

                // The method weather.getWeather() should block while waiting for the information
                // to be downloaded for server.
                if (weather.getWeather(location, date)) {
                    weatherTextView.setText("Temperature: " + weather.getTemp() + " °C\n");
                    weatherTextView.append("RealFeel: " + weather.getRealFeel() + " °C\n");
                    weatherTextView.append("Humidity: " + weather.getHumidity() + " %\n");
                    weatherTextView.append("Wind Speed: " + weather.getWindSpeed() + " km/h\n");
                } else {
                    weatherTextView.setText("Unable to get weather info");
                }

            }
        });
    }

    // Construct the Calendar object from the Date and Time picker fragments
    private Calendar getDate(){
        Calendar date = DatePickerFragment.getCalendar();
        Calendar time = TimePickerFragment.getCalendar();

        date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));

        return date;
    }

    // This method should get the Location from user input
    private String getLocation() {
        // This is harcoded, but it probably should use the Google Maps API to get this information.
        return "Guadalajara, Mexico";
    }

    // Show the Time Picker fragment
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    // Show the Date Picker fragment
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
